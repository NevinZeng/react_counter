const Counter = ({onChange, count, index}) => {

    function handleIncrease(){
        onChange(index, count + 1)
    }

    function handleDecrease(){
        onChange(index, count - 1)
    }
    return (
        
        <div className="Counter">
            <button onClick={handleIncrease}>+</button>
            <span>{count}</span>
            <button onClick={handleDecrease}>-</button>
        </div>
    );
}
export default Counter;
