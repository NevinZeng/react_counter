import { useSelector } from "react-redux/es/hooks/useSelector"
const CounterGroupSum = () => {

    const counterList = useSelector((state) => state.counter.counterList)
    const sum = counterList.reduce((count, counter) => count + counter.value ,0)

    return (
        <div>
            sum: {sum}
        </div>
    )
}

export default CounterGroupSum
