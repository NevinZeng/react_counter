import { useSelector, useDispatch } from "react-redux"
import { updateCounterSize } from "./counterSlice"
const CounterSizeGenerator = () => {

    const dispatch = useDispatch()
    const counterList = useSelector((state) => state.counter.counterList)
    const size = counterList.length

    const handleChange = (event) => {
        console.log("handleChange")
        dispatch(updateCounterSize(Number(event.target.value)))
    }

    return (
        <div>
            size: <input type="number" value={size} onChange={handleChange} min={0} />
        </div>
    )
}

export default CounterSizeGenerator
