import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    counterList: [{"id":1, "value":0}]
  },
  reducers: {
    updateCounterSize: (state, action) => {
      const size = state.counterList.length
      if(size < action.payload){
        state.counterList.push({"id":size+1, "value":0})
      }else if(size > action.payload){
        state.counterList.pop()
      }
    },
    updateCounterList: (state, action) => {
      state.counterList = action.payload
    }
  },
})

export const { incrementByAmount, updateCounterSize, updateCounterList } = counterSlice.actions
export default counterSlice.reducer
