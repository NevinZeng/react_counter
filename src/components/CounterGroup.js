import Counter from "./Counter";
import { useSelector, useDispatch } from "react-redux";
import { updateCounterList } from "./counterSlice";

const CounterGroup = () => {

    const counterList = useSelector((state) => state.counter.counterList)
    const dispatch = useDispatch()

    const handleChange = (index, updateValue) => {
        const willUpdateCounterList = [...counterList]
        const updateCounter = {"id": willUpdateCounterList[index].id, "value":updateValue}
        willUpdateCounterList[index] = updateCounter
        dispatch(updateCounterList(willUpdateCounterList))
    }

    return (
        <div className="CounterGroup">
            {
                counterList.map((counter, index) => (
                    <Counter key={counter.id} count={counter.value} index={index} onChange={handleChange}></Counter>
                ))
            }
        </div>
    );
}
export default CounterGroup;
